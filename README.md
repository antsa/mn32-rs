# MN32-rs
A (simple) config tool for the Mionix Naos 3200 mouse for linux

(Rust impl of [mn32](https://codeberg.org/antsa/mn32))

# Features
*  Enable/Disable LEDs (configurable per DPI step)
*  Set DPI values (800/1600/3200)
*  Set polling rate (1000/500/250/125 Hz)
*  Custom button assignments for all 7 mouse buttons
   * [x] Left/Right click
   * [x] Scroll click
   * [x] Back/Forward
   * [x] DPI Up/Down/Roll
   * [x] Scroll Up/Down
   * [x] Disable button
   * [X] Map single keyboard key to a mouse button
   * [ ] TODO: Macros
*   Configuration file (`XDG_CONFIG_HOME/mn32/mn32.toml`)

# Dependencies
    - Rust toolchain
    - libusb
    - GTK4

