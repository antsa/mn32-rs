pub use gtk4::prelude::*;

use glib::{clone, Continue, MainContext, PRIORITY_DEFAULT};
pub use gtk4::Application;
use gtk4::{
    Align, ApplicationWindow, Box, Button, ButtonsType, CheckButton, DialogFlags, DropDown, Grid,
    HeaderBar, Label, MessageDialog, MessageType, Notebook, Orientation, Widget,
};

use mn32_rs_core::*;

use std::{rc::Rc, sync::Arc, thread};

/// Ui strings
pub const UI_DPI_RATE: &[&str] = &["800", "1600", "3200"];
pub const UI_POLL: &[&str] = &["1000", "500", "250", "125"];
pub const UI_BTN_FUNC: &[&str] = &[
    "Left click",
    "Right click",
    "Scroll click",
    "Navigate backwards",
    "Navigate forwards",
    "Dpi up",
    "Dpi down",
    "Dpi roll",
    "Scroll up",
    "Scroll down",
    "Disabled",
    "Keyboard key",
];
pub const UI_USB_HID_KEY: &[&str] = &[
    "a & A", "b & B", "c & C", "d & D", "e & E", "f & F", "g & G", "h & H", "i & I", "j & J",
    "k & K", "l & L", "m & M", "n & N", "o & O", "p & P", "q & Q", "r & R", "s & S", "t & T",
    "u & U", "v & V", "w & W", "x & X", "y & Y", "z & Z", "1 & !", "2 & @", "3 & #", "4 & $",
    "5 & %", "6 & ^", "7 & &", "8 & *", "9 & (", "0 & )",
];

/// Converts dropdown selection to dpi value
macro_rules! dpi_to_enum {
    ($x:expr) => {
        DpiRate::from_u32($x.selected()).unwrap()
    };
}
/// Converts dropdown selections to btn values
macro_rules! btnassgn_to_enum {
    ($x:expr) => {
        BtnAssgnment {
            func: BtnFunc::from_u32($x.0.selected()).unwrap(),
            key: match BtnFunc::from_u32($x.0.selected()).unwrap() {
                BtnFunc::BtnKbdKey => UsbHidKey::from_u32($x.1.selected() + 4),
                _ => None,
            },
        }
    };
}

/// Builds and shows main widget
///
/// # Arguments
/// * app - a gtk application
pub fn build_gui(app: &Application) {
    Gui::new(app).window.show();
}

/// Gui elements (buttons, dropdowns, checkbuttons, window)
struct Gui {
    led_chkbtns: [CheckButton; AMOUNT_LED],
    dpi_dropdown: [DropDown; AMOUNT_DPI],
    poll_dropdown: DropDown,
    btnassgn_dropdowns: [(DropDown, DropDown); AMOUNT_BTN],
    btn_apply_cfg: Button,
    btn_save_cfg: Button,
    window: ApplicationWindow,
}

impl Gui {
    /// Returns a new Gui
    fn new(app: &Application) -> Rc<Self> {
        let tmp: Rc<Self> = Rc::new(Self {
            led_chkbtns: [
                CheckButton::with_label("Red (DPI1)"),
                CheckButton::with_label("Green (DPI2)"),
                CheckButton::with_label("Blue (DPI3)"),
            ],
            dpi_dropdown: [
                create_dpi_dropdown(),
                create_dpi_dropdown(),
                create_dpi_dropdown(),
            ],
            poll_dropdown: create_poll_dropdown(),
            btnassgn_dropdowns: [
                create_btn_dropdown(),
                create_btn_dropdown(),
                create_btn_dropdown(),
                create_btn_dropdown(),
                create_btn_dropdown(),
                create_btn_dropdown(),
                create_btn_dropdown(),
            ],
            btn_apply_cfg: Button::with_label("Apply Configuration"),
            btn_save_cfg: Button::from_icon_name(Some("gtk-save")),
            window: ApplicationWindow::new(app),
        });
        tmp.window.set_resizable(false);
        tmp.window.set_titlebar(Some(&tmp.create_headerbar()));
        tmp.window.set_title(Some("MN32-rs"));
        tmp.window.set_child(Some(&tmp.create_notebook()));
        tmp.btn_apply_cfg.connect_clicked(
            clone!(@strong tmp => move |_| send_data(Arc::new(tmp.get_data()), &tmp.window)),
        );
        tmp.btn_save_cfg.connect_clicked(
            clone!(@strong tmp => move |_| save_data(&tmp.get_data(), &tmp.window)),
        );
        tmp.btnassgn_dropdowns.iter().for_each(|dropdowns| {
            dropdowns
                .0
                .connect_selected_notify(clone!(@strong dropdowns => move |_| {
                    if dropdowns.0.selected() == (BtnFunc::BtnKbdKey as u32)  {
                        dropdowns.1.set_visible(true);
                    } else {
                        dropdowns.1.set_visible(false);
                        dropdowns.1.set_selected(0);
                    }
                }));
        });
        tmp.set_data(&mn32_rs_core::read_from_cfgfile());
        tmp
    }

    /// Sets gui element properties according to mouse
    fn set_data(&self, data: &Mouse) {
        self.led_chkbtns
            .iter()
            .zip(data.led_effects.iter())
            .for_each(|(button, &value)| button.set_active(value));
        self.dpi_dropdown
            .iter()
            .zip(data.dpis.iter())
            .for_each(|(dropdown, value)| {
                dropdown.set_selected(value.to_u32().unwrap());
            });
        self.poll_dropdown
            .set_selected(data.poll_rate.to_u32().unwrap());
        self.btnassgn_dropdowns
            .iter()
            .zip(data.btn_assgns.iter())
            .for_each(|(dropdowns, btn_assgn)| {
                dropdowns.0.set_selected(btn_assgn.func.to_u32().unwrap());
                if let Some(key) = btn_assgn.key {
                    dropdowns.1.set_selected(key.to_u32().unwrap() - 4);
                    dropdowns.1.set_visible(true);
                }
            });
    }

    /// Returns mouse from gui elements
    ///
    /// # Arguments
    /// * `window` - Parent window for error MessageDialog
    fn get_data(&self) -> Mouse {
        Mouse {
            led_effects: [
                self.led_chkbtns[0].is_active(),
                self.led_chkbtns[1].is_active(),
                self.led_chkbtns[2].is_active(),
            ],
            dpis: [
                dpi_to_enum!(self.dpi_dropdown[0]),
                dpi_to_enum!(self.dpi_dropdown[1]),
                dpi_to_enum!(self.dpi_dropdown[2]),
            ],
            poll_rate: PollRate::from_u32(self.poll_dropdown.selected()).unwrap(),
            btn_assgns: [
                btnassgn_to_enum!(self.btnassgn_dropdowns[0]),
                btnassgn_to_enum!(self.btnassgn_dropdowns[1]),
                btnassgn_to_enum!(self.btnassgn_dropdowns[2]),
                btnassgn_to_enum!(self.btnassgn_dropdowns[3]),
                btnassgn_to_enum!(self.btnassgn_dropdowns[4]),
                btnassgn_to_enum!(self.btnassgn_dropdowns[5]),
                btnassgn_to_enum!(self.btnassgn_dropdowns[6]),
            ],
        }
    }

    /// Creates the headerbar for the application
    fn create_headerbar(&self) -> HeaderBar {
        let headerbar = HeaderBar::new();

        headerbar.pack_start(&self.btn_apply_cfg);
        headerbar.pack_end(&self.btn_save_cfg);

        headerbar
    }

    /// Creates the notebook for the application widget
    fn create_notebook(&self) -> Widget {
        let notebook = Notebook::new();

        notebook.append_page(&self.create_main_page(), Some(&Label::new(Some("Main"))));
        notebook.append_page(
            &self.create_btn_page(),
            Some(&Label::new(Some("Button Assignments"))),
        );

        notebook.upcast()
    }

    /// Creates the main page for the notebook
    fn create_main_page(&self) -> Widget {
        let page = Grid::new();
        page.set_row_spacing(6);
        page.set_column_spacing(6);

        let leds = Box::new(Orientation::Horizontal, 6);
        self.led_chkbtns.iter().for_each(|btn| leds.append(btn));
        page.attach(&create_label("LED effects"), 0, 0, 1, 1);
        page.attach(&leds, 1, 0, 1, 1);

        page.attach(&create_label("DPI 1 (Red)"), 0, 1, 1, 1);
        page.attach(&self.dpi_dropdown[0], 1, 1, 1, 1);
        page.attach(&create_label("DPI 2 (Green)"), 0, 2, 1, 1);
        page.attach(&self.dpi_dropdown[1], 1, 2, 1, 1);
        page.attach(&create_label("DPI 3 (Blue)"), 0, 3, 1, 1);
        page.attach(&self.dpi_dropdown[2], 1, 3, 1, 1);

        page.attach(&create_label("Polling Rate(Hz)"), 0, 4, 1, 1);
        page.attach(&self.poll_dropdown, 1, 4, 1, 1);

        page.set_margin_start(6);
        page.set_margin_end(6);
        page.set_margin_top(6);
        page.set_margin_bottom(6);

        page.upcast()
    }

    /// Creates the button assignments page for the notebook
    fn create_btn_page(&self) -> Widget {
        let page = Grid::new();
        page.set_row_spacing(6);
        page.set_column_spacing(6);

        page.attach(&create_label("Left click"), 0, 0, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[0]), 1, 0, 1, 1);
        page.attach(&create_label("Right click"), 0, 1, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[1]), 1, 1, 1, 1);
        page.attach(&create_label("Scroll click"), 0, 2, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[2]), 1, 2, 1, 1);
        page.attach(&create_label("Backwards"), 0, 3, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[3]), 1, 3, 1, 1);
        page.attach(&create_label("Forwards"), 0, 4, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[4]), 1, 4, 1, 1);
        page.attach(&create_label("Up"), 0, 5, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[5]), 1, 5, 1, 1);
        page.attach(&create_label("Down"), 0, 6, 1, 1);
        page.attach(&create_btn_box(&self.btnassgn_dropdowns[6]), 1, 6, 1, 1);

        page.set_margin_start(6);
        page.set_margin_end(6);
        page.set_margin_top(6);
        page.set_margin_bottom(6);

        page.upcast()
    }
}

/// Creates a GTK Label
///
/// # Arguments
/// * `str` - Text for label
fn create_label(label: &str) -> Widget {
    let label = Label::new(Some(label));
    label.set_halign(Align::Start);
    label.set_margin_start(6);
    label.set_margin_end(75);
    label.upcast()
}

/// Creates a DropDown for dpi rate
fn create_dpi_dropdown() -> DropDown {
    let dpi_dropdown = DropDown::from_strings(UI_DPI_RATE);
    dpi_dropdown.set_hexpand(true);
    dpi_dropdown
}

/// Creates a DropDown for polling rate
fn create_poll_dropdown() -> DropDown {
    let poll_dropdown = DropDown::from_strings(UI_POLL);
    poll_dropdown.set_hexpand(true);
    poll_dropdown
}

/// Creates two DropDown as tuple for mouse button assignments
fn create_btn_dropdown() -> (DropDown, DropDown) {
    let btnassgn_dropdown_fnc = DropDown::from_strings(UI_BTN_FUNC);
    btnassgn_dropdown_fnc.set_hexpand(true);

    let btnassgn_dropdown_key = DropDown::from_strings(UI_USB_HID_KEY);
    btnassgn_dropdown_key.set_width_request(100);
    btnassgn_dropdown_key.set_visible(false);

    (btnassgn_dropdown_fnc, btnassgn_dropdown_key)
}

/// Creates a GTK Box with two DropDown for button assignments
///
/// # Arguments
/// * `(fnc, key)` - DropDown tuple
fn create_btn_box((fnc, key): &(DropDown, DropDown)) -> Box {
    let btn_box = Box::new(Orientation::Horizontal, 6);
    btn_box.append(fnc);
    btn_box.append(key);

    btn_box
}

/// Saves configuration to disk
///
/// Shows MessageDialog on Error
///
/// # Arguments
/// * `data` - Mouse
/// * `window`- ApplicationWindow for MessageDialog
fn save_data(data: &Mouse, window: &ApplicationWindow) {
    if let Err(err) = mn32_rs_core::save_to_cfgfile(data) {
        show_msg_dialog(
            format!("Could not save configuration to file:\n{}", err).as_str(),
            window,
        );
    }
}

/// Send configuration to mouse and save to disk
///
/// Uses a new thread for transfer and shows MessageDialogs on errors
///
/// # Arguments
/// * `data` - Mouse
/// * `window` - ApplicationWindow for MessageDialog
fn send_data(data: Arc<Mouse>, window: &ApplicationWindow) {
    let (mpsc_send, mpsc_recv) = MainContext::channel::<Result<(), Error>>(PRIORITY_DEFAULT);

    window.set_sensitive(false);
    window.set_cursor_from_name(Some("wait"));
    thread::spawn(
        clone!(@strong data => move || mpsc_send.send(mn32_rs_core::send_to_mouse(&data))),
    );

    mpsc_recv.attach(
        None,
        clone!(@strong window => @default-return Continue(false), move |result| {
            window.set_cursor_from_name(Some("default"));
            window.set_sensitive(true);
            if let Err(err) = result {
                show_msg_dialog(format!("Error during usb transfer:\n{}", err).as_str(), &window);
            } else {
                save_data(&data, &window);
                show_msg_dialog("Configuration successfully transfered!", &window);
            }
            Continue(true)
        }),
    );
}

/// Shows a modal MessageDialog ontop of window
///
/// # Arguments
/// * `txt` - Text of dialog
/// * `window` - Parent of MessageDialog
fn show_msg_dialog(txt: &str, window: &ApplicationWindow) {
    let message = MessageDialog::new(
        Some::<&ApplicationWindow>(window),
        DialogFlags::MODAL,
        MessageType::Info,
        ButtonsType::Ok,
        txt,
    );
    message.connect_response(|message, _| message.close());
    message.show();
}
