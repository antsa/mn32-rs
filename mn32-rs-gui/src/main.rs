//! Handling gtk gui
use mn32_rs_gui::*;

/// Setup and starts gui
fn main() {
    let application = Application::new(Some("org.codeberg.antsa.mn32-rs"), Default::default());
    application.connect_activate(|app| {
        build_gui(app);
    });
    application.run();
}
