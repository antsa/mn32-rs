//! Abstraction layer for ui

mod configfile;
mod mouse;
mod usb;

use std::io;

#[macro_use]
extern crate enum_primitive_derive;
extern crate num_traits;

pub use mouse::{
    BtnAssgnment, BtnFunc, DpiRate, Mouse, PollRate, UsbHidKey, AMOUNT_BTN, AMOUNT_DPI, AMOUNT_LED,
};
pub use num_traits::{FromPrimitive, ToPrimitive};
pub use usb::Error;

/// Returns ui data based on mouse read from config file
pub fn read_from_cfgfile() -> Mouse {
    configfile::read_from_file()
}

/// Saves configuration to config file
///
/// Returns success of operation
///
/// # Arguments
/// * `data` - ui data
pub fn save_to_cfgfile(data: &Mouse) -> io::Result<()> {
    configfile::save_to_file(data)
}

/// Sends configuration to mouse
///
/// Returns success of operation
///
/// # Arguments
/// * `data` - ui data
pub fn send_to_mouse(data: &Mouse) -> Result<(), Error> {
    usb::apply_configuration(&data.get_transfer_strings())
}
