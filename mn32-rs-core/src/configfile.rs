//! Reading/writing from/to configuration files

use crate::mouse::Mouse;
use std::{fs, io, path};

/// Prefix for xdg paths
const XDG_PREFIX: &str = "mn32";
/// Name of configuration file
const CFG_FILE_NAME: &str = "mn32.toml";

/// Returns a new mouse with values read from configuration file
///
/// Returns a default mouse if reading fails
pub fn read_from_file() -> Mouse {
    get_file_path()
        .ok()
        .and_then(|path| fs::read_to_string(&path).ok())
        .and_then(|file| toml::from_str(&file).ok())
        .unwrap_or_default()
}

/// Saves values of mouse into configuration file
///
/// Returns success of operation
///
/// # Arguments
///* `mouse` - A reference to a mouse
pub fn save_to_file(mouse: &Mouse) -> io::Result<()> {
    toml::to_string_pretty(mouse)
        .map_err(|_| io::Error::from(io::ErrorKind::InvalidData))
        .and_then(|contents| get_file_path().and_then(|path| fs::write(&path, &contents)))
}

/// Returns a xdg-compliant path to configuration file
fn get_file_path() -> io::Result<path::PathBuf> {
    xdg::BaseDirectories::with_prefix(XDG_PREFIX)
        .map_err(|err| err.into())
        .and_then(|dir| dir.place_config_file(CFG_FILE_NAME))
}
