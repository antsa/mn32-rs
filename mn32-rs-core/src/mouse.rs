//! Managing mouse configuration

use crate::usb::TransferString;

use num_traits::ToPrimitive;
use serde::{Deserialize, Serialize};
use std::vec::Vec;

/// Amount of dpi steps of mouse
pub const AMOUNT_DPI: usize = 3;
/// Amount of leds of mouse
pub const AMOUNT_LED: usize = 3;
/// Amount of buttons on mouse
pub const AMOUNT_BTN: usize = 7;

/// Pre-Prefix for usb transfer strings
const USB_PREFIX: u8 = 0x03;
/// Apply prefix for usb transfer string
const USB_APPLY: u8 = 0xcb;
/// LED prefix for usb transfer string
const USB_LED: u8 = 0xcd;
/// DPI prefix (step 1&2) for usb transfer string
const USB_DPI12: u8 = 0xc0;
/// DPI prefix (step 3) for usb transfer string
const USB_DPI3: u8 = 0xc1;
/// Polling rate prefix for usb transfer string
const USB_POLLRATE: u8 = 0xce;
/// Button prefix (buttons 1-5, first) for usb transfer strings
const USB_BTNA1: u8 = 0xc7;
/// Button prefix (buttons 1-5, second) for usb transfer strings
const USB_BTNA2: u8 = 0xc9;
/// Button prefix (buttons 6-7, first) for usb transfer strings
const USB_BTNB1: u8 = 0xc8;
/// Button prefix (buttons 6-7, second) for usb transfer strings
const USB_BTNB2: u8 = 0xca;

#[derive(Copy, Clone, Serialize, Deserialize, Primitive)]
/// Available dpi steps
pub enum DpiRate {
    /// 800 dpi (default value)
    Dpi800 = 0,
    /// 1600 dpi
    Dpi1600 = 1,
    /// 3200 dpi
    Dpi3200 = 2,
}
impl Default for DpiRate {
    fn default() -> Self {
        Self::Dpi800
    }
}
/// Available poll rates
#[derive(Serialize, Deserialize, Primitive)]
pub enum PollRate {
    /// 1000 Hz (default value)
    Poll1000 = 0,
    /// 500 Hz
    Poll500 = 1,
    /// 250 Hz
    Poll250 = 2,
    /// 125 Hz
    Poll125 = 3,
}
impl Default for PollRate {
    fn default() -> Self {
        Self::Poll1000
    }
}
/// Available options for button (function) assignments
#[derive(Serialize, Deserialize, Primitive)]
pub enum BtnFunc {
    /// Left click
    BtnLeftClick = 0,
    /// Right click
    BtnRightClick = 1,
    /// Scroll click
    BtnScrollClick = 2,
    /// Backwards
    BtnBackwards = 3,
    /// Forwards
    BtnForwards = 4,
    /// Dpi up
    BtnDpiUp = 5,
    /// Dpi down
    BtnDpiDown = 6,
    /// Dpi roll
    BtnDpiRoll = 7,
    /// Scroll up
    BtnScrollUp = 8,
    /// Scroll down
    BtnScrollDown = 9,
    /// Disable button
    BtnDisabled = 10,
    /// Usb hid (kbd) key
    BtnKbdKey = 11,
}
/// Available options for usb hid keys (kbd scan codes)
#[derive(Copy, Clone, Serialize, Deserialize, Primitive)]
pub enum UsbHidKey {
    /// a&A (default value)
    KeyA = 4,
    /// b&B
    KeyB = 5,
    /// c&C
    KeyC = 6,
    /// d&D
    KeyD = 7,
    /// e&E
    KeyE = 8,
    /// f&F
    KeyF = 9,
    /// g&G
    KeyG = 10,
    /// h&H
    KeyH = 11,
    /// i&I
    KeyI = 12,
    /// j&J
    KeyJ = 13,
    /// k&K
    KeyK = 14,
    /// l&L
    KeyL = 15,
    /// m&M
    KeyM = 16,
    /// n&N
    KeyN = 17,
    /// o&O
    KeyO = 18,
    /// p&P
    KeyP = 19,
    /// q&Q
    KeyQ = 20,
    /// r&R
    KeyR = 21,
    /// s&S
    KeyS = 22,
    /// t&T
    KeyT = 23,
    /// u&U
    KeyU = 24,
    /// v&V
    KeyV = 25,
    /// w&W
    KeyW = 26,
    /// x&X
    KeyX = 27,
    /// y&Y
    KeyY = 28,
    /// z&Z
    KeyZ = 29,
    /// 1&!
    Key1 = 30,
    /// 2&@
    Key2 = 31,
    /// 3&#
    Key3 = 32,
    /// 4&$
    Key4 = 33,
    /// 5&%
    Key5 = 34,
    /// 6&^
    Key6 = 35,
    /// 7&&
    Key7 = 36,
    /// 8&*
    Key8 = 37,
    /// 9&(
    Key9 = 38,
    /// 0&)
    Key0 = 39,
}
impl Default for UsbHidKey {
    fn default() -> Self {
        Self::KeyA
    }
}
#[derive(Serialize, Deserialize)]
pub struct BtnAssgnment {
    pub func: BtnFunc,
    pub key: Option<UsbHidKey>,
}
impl BtnAssgnment {
    /// Returns usb code for button assignment usb transfer string
    fn get_codes(&self) -> (u8, u8) {
        match self.func {
            BtnFunc::BtnLeftClick => (0x00, 0x01),
            BtnFunc::BtnRightClick => (0x00, 0x02),
            BtnFunc::BtnScrollClick => (0x00, 0x04),
            BtnFunc::BtnBackwards => (0x00, 0x08),
            BtnFunc::BtnForwards => (0x00, 0x10),
            BtnFunc::BtnDpiUp => (0x04, 0x01),
            BtnFunc::BtnDpiDown => (0x04, 0x02),
            BtnFunc::BtnDpiRoll => (0x04, 0x00),
            BtnFunc::BtnScrollUp => (0x03, 0x01),
            BtnFunc::BtnScrollDown => (0x03, 0xff),
            BtnFunc::BtnDisabled => (0x05, 0x00),
            BtnFunc::BtnKbdKey => (0x02, self.key.unwrap().to_u8().unwrap()),
        }
    }
}

/// Generate usb transfer strings
trait UsbTransfer {
    /// Returns a vector of strings for usb transfer
    fn add_transfer_strings(&self, vector: &mut Vec<TransferString>);
}
impl UsbTransfer for [bool; AMOUNT_LED] {
    fn add_transfer_strings(&self, vector: &mut Vec<TransferString>) {
        vector.push([
            USB_PREFIX,
            USB_LED,
            (self[0] as u8),
            (self[1] as u8) * 2,
            (self[2] as u8) * 3,
            0,
            0,
            0,
        ]);
    }
}
impl UsbTransfer for [DpiRate; AMOUNT_DPI] {
    fn add_transfer_strings(&self, vector: &mut Vec<TransferString>) {
        vector.push([
            USB_PREFIX,
            USB_DPI12,
            0x03 << (self[0].to_u8().unwrap()),
            0x20 << (self[0].to_u8().unwrap()),
            0x03 << (self[1].to_u8().unwrap()),
            0x20 << (self[1].to_u8().unwrap()),
            0,
            0,
        ]);
        vector.push([
            USB_PREFIX,
            USB_DPI3,
            0x03 << (self[2].to_u8().unwrap()),
            0x20 << (self[2].to_u8().unwrap()),
            0,
            0,
            0,
            0,
        ]);
    }
}
impl UsbTransfer for PollRate {
    fn add_transfer_strings(&self, vector: &mut Vec<TransferString>) {
        vector.push([
            USB_PREFIX,
            USB_POLLRATE,
            self.to_u8().unwrap() + 1,
            0,
            0,
            0,
            0,
            0,
        ]);
    }
}
impl UsbTransfer for [BtnAssgnment; AMOUNT_BTN] {
    fn add_transfer_strings(&self, vector: &mut Vec<TransferString>) {
        let mut reqa1: TransferString = [USB_PREFIX, USB_BTNA1, 0, 0, 0, 0, 0, 0];
        let mut reqa2: TransferString = [USB_PREFIX, USB_BTNA2, 0, 0, 0, 0, 0, 0];
        self[0..=4].iter().enumerate().for_each(|(i, button)| {
            let (upper, lower) = button.get_codes();
            reqa1[i + 2] = upper;
            reqa2[i + 2] = lower;
        });
        let mut reqb1: TransferString = [USB_PREFIX, USB_BTNB1, 0, 0, 0, 0, 0, 0];
        let mut reqb2: TransferString = [USB_PREFIX, USB_BTNB2, 0, 0, 0, 0, 0, 0];
        self[5..=6].iter().enumerate().for_each(|(i, button)| {
            let (upper, lower) = button.get_codes();
            reqb1[i + 2] = upper;
            reqb2[i + 2] = lower;
        });
        vector.push(reqa1);
        vector.push(reqb1);
        vector.push(reqa2);
        vector.push(reqb2);
    }
}

/// Mouse configuration
#[derive(Serialize, Deserialize)]
pub struct Mouse {
    /// Led configurations
    pub led_effects: [bool; AMOUNT_LED],
    /// Dpi step configurations
    pub dpis: [DpiRate; AMOUNT_DPI],
    /// Poll rate configuration
    pub poll_rate: PollRate,
    /// Button assignments
    pub btn_assgns: [BtnAssgnment; AMOUNT_BTN],
}
impl Mouse {
    /// Returns a vector of usb transfer strings of configuration
    pub fn get_transfer_strings(&self) -> Vec<TransferString> {
        let mut vector: Vec<TransferString> = Vec::new();
        self.btn_assgns.add_transfer_strings(&mut vector);
        self.led_effects.add_transfer_strings(&mut vector);
        self.poll_rate.add_transfer_strings(&mut vector);
        self.dpis.add_transfer_strings(&mut vector);
        vector.push([USB_PREFIX, USB_APPLY, 0, 0, 0, 0, 0, 0]); // Apply configuration
        vector
    }
}
impl Default for Mouse {
    fn default() -> Self {
        Self {
            led_effects: [Default::default(); AMOUNT_LED],
            dpis: [Default::default(); AMOUNT_DPI],
            poll_rate: Default::default(),
            btn_assgns: [
                BtnAssgnment {
                    func: BtnFunc::BtnLeftClick,
                    key: None,
                },
                BtnAssgnment {
                    func: BtnFunc::BtnRightClick,
                    key: None,
                },
                BtnAssgnment {
                    func: BtnFunc::BtnScrollClick,
                    key: None,
                },
                BtnAssgnment {
                    func: BtnFunc::BtnBackwards,
                    key: None,
                },
                BtnAssgnment {
                    func: BtnFunc::BtnForwards,
                    key: None,
                },
                BtnAssgnment {
                    func: BtnFunc::BtnDpiUp,
                    key: None,
                },
                BtnAssgnment {
                    func: BtnFunc::BtnDpiDown,
                    key: None,
                },
            ],
        }
    }
}
