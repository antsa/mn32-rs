//! Usb transfer of mouse configuration

use rusb::{DeviceHandle, Direction, GlobalContext, Recipient, RequestType};
use std::{thread, time::Duration};

/// Delay between usb transfer
const XFER_WAIT: Duration = Duration::from_millis(50);
/// Transfer timeout (ms)
const XFER_TIMEOUT: Duration = Duration::from_millis(50);
/// Value for setup package
const XFER_SETUP_PKG: u16 = 0x0303;
/// Size of transfer string
const XFER_SIZE: usize = 8;

/// Device vendor id
pub const DEVICE_VID: u16 = 0x22d4;
/// Device product id
pub const DEVICE_PID: u16 = 0x1100;

/// Alias for a usb transfer string
pub type TransferString = [u8; XFER_SIZE];

/// Enum for error handling
pub enum Error {
    Rusb(rusb::Error),
    TransferError,
    MouseNotFoundError,
}
impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            Error::Rusb(x) => x.fmt(fmt),
            Error::TransferError => fmt.write_str("Transfer error, mouse response malformed"),
            Error::MouseNotFoundError => {
                fmt.write_str("Mouse could not be found! (Maybe unplugged?)")
            }
        }
    }
}
impl std::convert::From<rusb::Error> for Error {
    fn from(error: rusb::Error) -> Self {
        Self::Rusb(error)
    }
}

/// Applies mouse configuration
///
/// Returns success of operation
///
/// # Arguments
/// * `strings` - A vector of configuration strings
pub fn apply_configuration(strings: &[TransferString]) -> Result<(), Error> {
    let mut handle =
        rusb::open_device_with_vid_pid(DEVICE_VID, DEVICE_PID).ok_or(Error::MouseNotFoundError)?;

    handle.set_auto_detach_kernel_driver(true)?;

    handle.claim_interface(0)?;

    strings
        .iter()
        .try_for_each(|value| send_to_mouse(&mut handle, value))
        .map_err(|x| {
            handle.release_interface(0).ok();
            x
        })?;

    handle.release_interface(0)?;
    Ok(())
}

/// Send transfer string to mouse
///
/// Returns success of operation
///
/// # Arguments
/// * `handle` - A libusb device handle
/// * `buf` - A configuration string
fn send_to_mouse(
    handle: &mut DeviceHandle<GlobalContext>,
    buf: &TransferString,
) -> Result<(), Error> {
    handle.reset()?;

    if handle.write_control(
        rusb::request_type(Direction::Out, RequestType::Class, Recipient::Interface),
        9,
        XFER_SETUP_PKG,
        0,
        buf,
        XFER_TIMEOUT,
    )? != XFER_SIZE
    {
        return Err(Error::TransferError);
    }

    thread::sleep(XFER_WAIT);

    if handle.read_control(
        rusb::request_type(Direction::In, RequestType::Class, Recipient::Interface),
        1,
        XFER_SETUP_PKG,
        0,
        &mut [0; XFER_SIZE],
        XFER_TIMEOUT,
    )? != XFER_SIZE
    {
        Err(Error::TransferError)
    } else {
        Ok(())
    }
}
